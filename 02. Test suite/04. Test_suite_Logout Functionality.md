># Test Suite - Logout Functionality

**TC_04_01**  
**Title:** Check if the link of Logout redirects to Login page  
**Priority:** 2  
**Description:**  User can logout from application  
**Prerequisites:** There is a logged in user and aplication is open at home page is loaded 

**Steps:**
1. Click on "Logout" hyperlink

**Expected result:** The user has logged out of the application and  has been redirected to Login page  

***

**TC_04_02**  
**Title:** Check if Logout works correctly when using the "Back" button  
**Priority:** 2  
**Description:**  
**Prerequisites:** There is a logged in user and aplication is open at home page is loaded 

**Steps:**
1. Click on "Logout" hyperlink 
2. Click Browser's back button

**Expected result:** User is still logged out and on current page Sign in and Register buttons are visible  

***

**TC_04_03**  
**Title:** Check if a guest can see the Logout button   
**Priority:** 2  
**Description:**  Guest can not see Logout button  
**Prerequisites:**  

**Steps:**
 1. Go to http://localhost:8081  

**Expected result:** The Logout button is not visible