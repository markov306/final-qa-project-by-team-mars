># Test Suite - Post Functionality

**TC_07_01**  
**Title:**  Create a public post   
**Priority:** 1   
**Description:**  As a registered user I want to create a public post  
**Prerequisites:**  The user is registered and logged in   
**Steps:**  
1. Click on Add New post button   
2. On Post's visibility set Public post
3. Fill some data in Message field   
4. Add picture
5. Click on Save post button  

**Expected result:**  The post is created. The post is public and visible for registered and unregistered users

***
**TC_07_02**  
**Title:**  Create a private post   
**Priority:** 1   
**Description:**  As a registered user I want to create a private post  
**Prerequisites:**  The user is registered and logged in   
**Steps:**  
1. Click on Add New post button   
2. On Post's visibility set Private post
3. Fill some data in Message field   
4. Add picture
5. Click on Save post button  

**Expected result:**  The private post is created.  Only my connections and admins can see it

***
**TC_07_03**  
**Title:**  Create a post as a unregistered user  
**Priority:** 1   
**Description:**  As a unegistered user I want to create a public post  
**Prerequisites:**  N/A  
**Steps:**  
1. Click on Add New post button   
2. On Post's visibility set Public post
3. Don't fill any information in Message field   
4. Don't add picture
5. Click on Save post button  

**Expected result:**  The post is not created. An error message appears stating that an empty post cannot be created.

***
**TC_07_04**  
**Title:**  Create a post with empty body   
**Priority:** 1   
**Description:**  As a registered user I want to create a public post with empty title    
**Prerequisites:**  The user is registered and logged in   
**Steps:**  
1. Click on Add New post button   
2. On Post's visibility set Public post
3. Don't fill any information in Message field   
4. Don't add picture
5. Click on Save post button  

**Expected result:**  The post is not created. An error message appears stating that an empty post cannot be created.

***
**TC_07_05**  
**Title:**  Create a post with length one symbol   
**Priority:** 1   
**Description:**  As a registered user I want to create a public post with length one symbol    
**Prerequisites:**  The user is registered and logged in   
**Steps:**  
1. Click on Add New post button   
2. On Post's visibility set Public post
3. Fill one symbol in Message field   
4. Don't add picture
5. Click on Save post button  

**Expected result:**  А minimum post length error message should appear

***
**TC_07_06**  
**Title:**  Create a post with length 1000 continuous symbols   
**Priority:** 1   
**Description:**  As a registered user I want to create a public post with length 1000 continuous symbols    
**Prerequisites:**  The user is registered and logged in   
**Steps:**  
1. Click on Add New post button   
2. On Post's visibility set Public post
3. Fill 1000 numbers in Message field   
4. Don't add picture
5. Click on Save post button  

**Expected result:**  The post is created. The post is public and visible for registered and unregistered users

***
**TC_07_07**  
**Title:**  Create a post with total text length 1000 symbols   
**Priority:** 1   
**Description:**  As a registered user I want to create a public post with total text length 1000 symbols      
**Prerequisites:**  The user is registered and logged in   
**Steps:**  
1. Click on Add New post button   
2. On Post's visibility set Public post
3. Fill 1000 numbers in Message field   
4. Don't add picture
5. Click on Save post button  

**Expected result:**  The post is created. The post is public and visible for registered and unregistered users

***
**TC_07_08**  
**Title:**  The user edits his own post  
**Priority:** 1   
**Description:**  As a registered user I want edit my own post    
**Prerequisites:**  The user is registered, logged and has a post created   
**Steps:**  
1. Click on Latest post button   
2. Find user own post and click on Explore this post button
3. Click on Edit post button  
4. Edit post body
5. Click on Save post button  

**Expected result:**  When I edit my post I can see its contents

***
**TC_07_09**  
**Title:**  The user edit picture in his own post   
**Priority:** 1   
**Description:**  As a registered user I want edit picture in my own post    
**Prerequisites:**  The user is registered, logged and has a post with picture created   
**Steps:**  
1. Click on Latest post button   
2. Find user own post and click on Explore this post button
3. Click on Edit post button  
4. Edit post picture
5. Click on Save post button  

**Expected result:**  When I edit my post I can see the uploaded picture

***
**TC_07_10**  
**Title:**  The user delete his own post   
**Priority:** 1   
**Description:**  As a registered user I want delete my own post    
**Prerequisites:**  The user is registered, logged and has a post created   
**Steps:**  
1. Click on Latest post button   
2. Find user own post and click on Explore this post button
3. Click on Delete post button  
4. In drop down menu set Delete post
5. Click on Submit button  

**Expected result:**  The post is delete

***
**TC_07_11**  
**Title:**  Abort post deleting   
**Priority:** 1   
**Description:**  As a registered user I want delete my own post    
**Prerequisites:**  The user is registered, logged and has a public post created   
**Steps:**  
1. Click on Latest post button   
2. Find user own post and click on Explore this post button
3. Click on Delete post button  
4. In drop down menu set Cancel
5. Click on Submit button  

**Expected result:**  The post is still visible for registered and unregistered users

***
**TC_07_12**  
**Title:**  See my post without a photo attached  
**Priority:** 2   
**Description:**  As a registered user I want view created post without picture    
**Prerequisites:**  The user is registered, logged and has a public post created   
**Steps:**  
1. Click on Latest post button   
 
**Expected result:**  view created post without picture    

***
**TC_07_13**  
**Title:**  See my post picture when I edit my post   
**Priority:** 2   
**Description:**  View attached photo when editing my post  
**Prerequisites:**  The user is registered, logged and has a public post whith photo created   
**Steps:**  
1. Click on Latest post button   
2. Click on Explore this post button on my post
3. Click Edit post button
 
**Expected result:**  View attached photo when editing my post   