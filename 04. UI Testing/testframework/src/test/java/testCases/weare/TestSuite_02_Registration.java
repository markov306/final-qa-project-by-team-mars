package testCases.weare;

import com.telerikacademy.testframework.Utils;
import org.junit.Test;

public class TestSuite_02_Registration extends BaseTest {

    @Test
    public void TC_02_01_SuccessfulRegistration() {
        registerPage.successfulRegistration();
        registerPage.assertWelcomeMessageVisible();
    }

    @Test
    public void TC_02_04_FailedRegistrationWithEmptyField() {
        registerPage.registrationWithEmptyField();
        registerPage.assertJoinMessageVisible();
    }

    @Test
    public void TC_02_05_FailedRegistrationWithOneCharacterForUsername() {
        registerPage.registration_With_Data_To_be_filled_in("Z",
                "valen@abv.bg", "trello", "trello");
        registerPage.assertJoinMessageIsNotVisible();
    }

    @Test
    public void TC_02_05_RegistrationWithFiftyCharacterForUsername() {
        registerPage.registration_With_Data_To_be_filled_in("dQuNNqrzfwczJKEZSKwtoOSwqDYuXwTJXfGGuCGMRJqHJkwspQ",
                "valen@abv.bg", "trello", "trello");
        registerPage.assertJoinMessageIsNotVisible();
    }

    @Test
    public void TC_02_06_FailedRegistrationWithInvalidEmailFormat() {
        registerPage.registration_With_Data_To_be_filled_in("Front",
                "valensia.bg", "trello", "trello");
        registerPage.assertInvalidEmailMessageVisible();
    }

}

