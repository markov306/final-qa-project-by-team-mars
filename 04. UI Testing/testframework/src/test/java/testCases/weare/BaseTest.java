package testCases.weare;

import com.telerikacademy.testframework.UserActions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import pages.weare.*;

public class BaseTest {


	UserActions actions = new UserActions();

	@BeforeClass
	public static void setUp(){
		UserActions.loadBrowser("weare.homePage");
	}

	@AfterClass
	public static void tearDown(){
		UserActions.quitDriver();
	}

	WeareAdminPage adminPage = new WeareAdminPage(actions.getDriver());
	WeareAdminAllUsersPage adminAllUsersPage = new WeareAdminAllUsersPage(actions.getDriver());
	WeareHomePage home = new WeareHomePage(actions.getDriver());
	WeareLoginPage loginPage = new WeareLoginPage(actions.getDriver());
	WeareRegisterPage registerPage = new WeareRegisterPage(actions.getDriver());
	WeareProfileEditorPage profileEditorPage = new WeareProfileEditorPage(actions.getDriver());
	WeareNewPostPage newPostPage = new WeareNewPostPage(actions.getDriver());
	WeareExploreAllPostsPage exploreAllPostsPage = new WeareExploreAllPostsPage(actions.getDriver());
	WeareSendFriendshipRequestPage sendRequestPage = new WeareSendFriendshipRequestPage(actions.getDriver());


}
