package testCases.weare;

import org.junit.Test;

public class TestSuite_08_Comments extends BaseTestWithLogOut {


    @Test
    public void TC_08_01_CreateCommentOnPost() {
        loginPage.login("user1.username", "user1.password");
        exploreAllPostsPage.createComment();
        exploreAllPostsPage.assertCommentContentVisible();
    }

}

