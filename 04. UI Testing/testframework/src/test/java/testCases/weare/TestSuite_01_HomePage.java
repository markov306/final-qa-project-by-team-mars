package testCases.weare;

import org.junit.Test;

public class TestSuite_01_HomePage extends BaseTest {

    @Test
    public void TC_01_05_CheckRegisterBtnOnPageHeader() {
        home.checkRegisterBtnOnHeader();
        home.assertJoinMessageVisible();
    }

    @Test
    public void TC_01_07_CheckSignBtnOnPageHeader() {
        home.checkSignBtnOnHeader();
        home.assertUsernameFieldVisible();
    }

    @Test
    public void TC_01_11_CheckAboutUsBtnOnPageHeader() {
        home.checkAboutUsBtnOnHeader();
        home.assertAboutUsLogoVisible();
    }

}

