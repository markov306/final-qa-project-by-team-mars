package testCases.weare;

import com.telerikacademy.testframework.UserActions;
import org.junit.After;

public class BaseTestWithLogOut extends BaseTest{
    UserActions actions = new UserActions();

    @After
    public void afterTest() {
        loginPage.logOut();
    }
}
