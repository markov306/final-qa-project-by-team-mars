package testCases.weare;

import org.junit.Test;

public class TestSuite_04_Logout extends BaseTest {

    @Test
    public void TC_04_01_SuccessfulLogoutAndRedirectToLoginPage() {
        loginPage.login("user1.username", "user1.password");
        loginPage.logOut();
        loginPage.assertLogoutMessageVisible();
    }

    @Test
    public void TC_04_03_LogoutBtnIsNotVisibleForGuest() {
        home.assertLogoutBtnIsNotVisible();
    }

}

