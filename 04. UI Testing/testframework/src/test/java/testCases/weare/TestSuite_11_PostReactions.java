package testCases.weare;

import org.junit.Test;

public class TestSuite_11_PostReactions extends BaseTest {

    @Test
    public void TC_11_01_LikePublicPost() {
        loginPage.login("user2.username", "user2.password");
        newPostPage.likePost();
        newPostPage.assertDislikeBtnVisible();
        loginPage.logOut();
    }

    @Test
    public void TC_11_05_LikePublicPostAsGuest() {
        loginPage.login("empty.username", "empty.password");
        newPostPage.likePostAsGuest();
        newPostPage.assertLikeBtnIsNotVisible();
    }
}

