package testCases.weare;

import org.junit.Test;

public class TestSuite_12_SearchEngine extends BaseTest {

    @Test
    public void TC_12_01_FailedSearchJobAndName() {
        home.searchUserByJobAndName();
        home.assertNoExistingUserMsgVisible();
    }

    @Test
    public void TC_12_02_SearchJob() {
        home.searchUserByJob();
        home.assertResultByJobIsVisible();
    }

    @Test
    public void TC_12_03_SearchName() {
        home.searchUserByName();
        home.assertResultByNameIsVisible();
    }

}

