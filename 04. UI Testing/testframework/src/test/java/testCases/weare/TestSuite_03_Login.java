package testCases.weare;

import org.junit.Test;

public class TestSuite_03_Login extends BaseTest {

    @Test
    public void TC_03_01_SuccessfulLoginWithUsernameAndPassword() {
        loginPage.login("user1.username", "user1.password");
        home.assertNewPostVisible();
        loginPage.logOut();
    }

    @Test
    public void TC_03_02_FailedLoginWithValidUsernameAndInvalidPassword() {
        loginPage.login("user1.username", "invalid.password");
        loginPage.assertWrongPassMsgVisible();
    }

    @Test
    public void TC_03_03_FailedLoginWithInvalidUsernameAndValidPassword() {
        loginPage.login("invalid.username", "user1.password");
        loginPage.assertWrongPassMsgVisible();
    }

    @Test
    public void TC_03_04_FailedLoginWithEmptyUsernameAndEmptyPassword() {
        loginPage.login("empty.username", "empty.password");
        loginPage.assertWrongPassMsgVisible();
    }

}


