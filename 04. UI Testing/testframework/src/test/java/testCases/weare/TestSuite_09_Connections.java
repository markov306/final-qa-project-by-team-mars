package testCases.weare;

import org.junit.Test;

public class TestSuite_09_Connections extends BaseTestWithLogOut {

/*    @Test
    public void TC_09_03_FriendshipRequestNotification() {
        loginPage.login("user2.username", "user2.password");
    }*/

    @Test
    public void TC_09_05_SendFriendshipRequest() {
        loginPage.login("user2.username", "user2.password");
        sendRequestPage.sendFriendshipReq();
        sendRequestPage.assertConnectionMessageVisible();
    }

}

