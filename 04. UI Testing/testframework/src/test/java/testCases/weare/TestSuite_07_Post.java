package testCases.weare;

import org.junit.Test;

public class TestSuite_07_Post extends BaseTestWithLogOut {


    @Test
    public void TC_07_01_SuccessfulPublicPostCreated() {
        loginPage.login("user1.username", "user1.password");
        newPostPage.createPost();
        newPostPage.assertPostContentVisible();
    }

    @Test
    public void TC_07_08_UserEditHisOwnPost() {
        loginPage.login("user1.username", "user1.password");
        newPostPage.editPost();
        newPostPage.assertEditPostContentVisible();
    }

}
