package testCases.weare;

import org.junit.Test;

public class TestSuite_06_AdminSettings extends BaseTestWithLogOut {


    @Test
    public void TC_06_03_AdminEditUserFirstName() {
        loginPage.login("admin.username", "admin.password");
        adminPage.adminEditUserFirstName();
        adminPage.assertNewUserFirstNameAdminEdit();
        adminPage.rollBackAdminEditUserFirstName();
    }

    @Test
    public void TC_06_07_DisableUser() {
        loginPage.login("admin.username", "admin.password");
        adminAllUsersPage.disableUser();
        adminAllUsersPage.assertEnableBtnVisible();
    }

    @Test
    public void TC_06_09_AdminEditUserPost() {
        loginPage.login("admin.username", "admin.password");
        adminAllUsersPage.adminEditUserPost();
        adminAllUsersPage.assertAdminEditPostVisible();
    }

}
