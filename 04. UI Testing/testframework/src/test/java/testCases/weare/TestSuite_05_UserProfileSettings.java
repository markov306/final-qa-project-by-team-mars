package testCases.weare;

import org.junit.Test;

public class TestSuite_05_UserProfileSettings extends BaseTestWithLogOut {

    @Test
    public void TC_05_01_ChangeUserFirstName() {
        loginPage.login("user2.username", "user2.password");
        profileEditorPage.successfulEditFirstName();
        profileEditorPage.assertNewUserFirstNameAfterEditProfile();
        profileEditorPage.rollBackUserFirstName();
    }

    @Test
    public void TC_05_02_ChangeUserLastName() {
        loginPage.login("user2.username", "user2.password");
        profileEditorPage.successfulEditLastName();
        profileEditorPage.assertNewUserLastNameAfterEditProfile();
        profileEditorPage.rollBackUserLastName();
    }

    @Test
    public void TC_05_08_ChangeUserDescription() {
        loginPage.login("user2.username", "user2.password");
        profileEditorPage.successfulEditUserDescription();
        profileEditorPage.assertNewUserDescriptionAfterEditProfile();
        profileEditorPage.rollBackUserDescription();
    }

    @Test
    public void TC_05_09_ChangeUserCityLocation() {
        loginPage.login("user1.username", "user1.password");
        profileEditorPage.successfulEditCity();
        profileEditorPage.assertNewCityAfterEditProfile();
    }

}
