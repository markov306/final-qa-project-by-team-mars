package testCases.weare;

import org.junit.Test;

public class Test_00_PreRequisiteTest extends BaseTest{

/*
    This test must be executed only once for clean Database.
    It purpose is to create all necessary data for tests.
*/

    @Test
    public void createInitialDataNeededForOtherTests(){

        //Register Admin
       registerPage.registrationWithPresetData("admin.username","admin.email",
               "admin.password","admin.password");
        loginPage.login("admin.username","admin.password");
        profileEditorPage.initialProfileDataUpdate("admin.firstName","admin.lastName","admin.birthDay");
        loginPage.logOut();

        //Register and update profile for user Manchev
        registerPage.registrationWithPresetData("user1.username","user1.email",
                "user1.password","user1.password");
        loginPage.login("user1.username","user1.password");
        profileEditorPage.initialProfileDataUpdate("user1.firstName","user1.lastName","user1.birthDay");
        profileEditorPage.initialProfileCategoryUpdate("user1.categoryCode");
        loginPage.logOut();

        //Register and update profile for user Gandalf
        registerPage.registrationWithPresetData("user2.username","user2.email",
                "user2.password","user2.password");
        loginPage.login("user2.username","user2.password");
        profileEditorPage.initialProfileDataUpdate("user2.firstName","user2.lastName","user2.birthDay");

    }

}
