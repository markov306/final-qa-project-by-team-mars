package testCases.weare;

import org.junit.Test;

public class TestSuite_10_PublicFeed extends BaseTest {

    @Test
    public void TC_10_01_RegisteredUserCanSeeAllPublicPosts() {
        loginPage.login("user2.username", "user2.password");
        newPostPage.viewPublicPost();
        newPostPage.assertPublicPostIsVisible();
        loginPage.logOut();
    }

    @Test
    public void TC_10_02_UnregisteredUserCanSeeAllPublicPosts() {
        newPostPage.guestCanViewPublicPost();
        newPostPage.assertPublicPostIsVisible();
    }
}

