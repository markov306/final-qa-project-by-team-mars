package com.telerikacademy.testframework;

import org.openqa.selenium.WebDriver;

import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Utils {

	private static Properties uiMappings =  PropertiesManager.PropertiesManagerEnum.INSTANCE.getUiMappings();
	private static Properties configProperties =  PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties();
	public static final Logger LOG = LogManager.getRootLogger();

	public static WebDriver getWebDriver() {
		LOG.info("Initializing WebDriver");
		return CustomWebDriverManager.CustomWebDriverManagerEnum.INSTANCE.getDriver();
	}

	public static void tearDownWebDriver() {
		LOG.info("Quitting WebDriver");
		CustomWebDriverManager.CustomWebDriverManagerEnum.INSTANCE.quitDriver();
	}

	public static String getUIMappingByKey(String key) {
		String value = uiMappings.getProperty(key);
		return value != null ? value : key;
	}

	public static Properties getConfigProperties() {
		return PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties();
	}

	public static String getConfigPropertyByKey(String key){
		return configProperties.getProperty(key);
	}

	public static String getRandomEmail(int n){
		StringBuilder sb = new StringBuilder(n);

		sb.append(getRandomString(n));
		sb.append("@abv.bg");

		return sb.toString();
	}

	public static String getRandomString(int n) {

		// chose a Character random from this String
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvxyz";

		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);

		for (int i = 0; i < n; i++) {

			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index
					= (int) (AlphaNumericString.length()
					* Math.random());

			// add Character one by one in end of sb
			sb.append(AlphaNumericString
					.charAt(index));
		}

		return sb.toString();
	}

	public static String getRandomIntFromRange(int min, int max){
		int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
		return Integer.toString(randomNum);
	}
}
