package com.telerikacademy.testframework;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserActions {
    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    public void goToUrl(String key, Object... arguments){
        String urlAddress = Utils.getConfigPropertyByKey(key);
        driver.get(urlAddress);
    }

    //###################### DROP DOWN ELEMENT ########################

    public void selectsDropDownItemByValue(String optionValue,String dropdownElementLocator){

        WebElement selectObject  = driver.findElement(By.xpath(Utils.getUIMappingByKey(dropdownElementLocator)));
        Select dropDownElement = new Select(selectObject);
        dropDownElement.selectByValue(optionValue);
    }

    //###################### CLICK ########################
    public void clickElement(String key, Object... arguments) {
        String locator = getLocatorValueByKey(key, arguments);
        waitForElementClickable(key);
        WebElement element = driver.findElement(By.xpath(locator));
        Utils.LOG.info("Clicking on element " + key);
        element.click();
    }

    public void resolveSVG(String key, Object... arguments) {
        String locator = getLocatorValueByKey(key, arguments);
        Utils.LOG.info("Clicking on element " + key);
        WebElement svg = driver.findElement(By.xpath(locator));
        Actions actions = new Actions(driver);
        actions.moveToElement(svg).click().build().perform();
    }

    //#################     NEW METHODS #####################

    public void typeValueInField(String value, String key) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        waitForElementClickable(key);
        Utils.LOG.info("Clears content of the element " + key);
        element.clear();
        Utils.LOG.info("Typing \"" + value + "\" into element " + key);
        element.sendKeys(value);
    }

    //############# TextEquals #########

    public void assertTextEquals(String expectedText, String locator) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
        String textOfWebElement = webElement.getText();
        Utils.LOG.info("Expected text: " + expectedText + "; Actual text: " + textOfWebElement);
        Assert.assertEquals(expectedText, textOfWebElement);
    }


    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = getLocatorValueByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }

    //############# WAITS #########

    public void waitForElementClickable(String key, Object... arguments) {
        String locator = getLocatorValueByKey(key, arguments);
        Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
        Utils.LOG.info("Wait for element " + key + " to be clickable");
        waitForElementClickableUntilTimeout(locator, defaultTimeout, arguments);
    }

    public void waitForElementClickableUntilTimeout(String locator, int seconds, Object... locatorArguments) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        String xpath = getLocatorValueByKey(locator, locatorArguments);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
            ;
        } catch (Exception exception) {
            Assert.fail("Element with locator: '" + xpath + "' was not found.");
        }
    }
    //###################################################################

    public void waitForElementIsVisible(String locatorKey, Object... arguments) {
        Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
        Utils.LOG.info("Wait for element " + locatorKey + " to be visible");
        waitForElementVisibleUntilTimeout(locatorKey, defaultTimeout, arguments);
    }

    public void waitForElementVisibleUntilTimeout(String locator, int seconds, Object... locatorArguments) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        String xpath = getLocatorValueByKey(locator, locatorArguments);
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));

        } catch (Exception exception) {
            Assert.fail("Element with locator: '" + xpath + "' was not found.");
        }
    }

     public void waitFor(long timeOutMilliseconds) {
        try {
            Thread.sleep(timeOutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //############# ASSERTS #########
    public void assertElementContainsText(String text, String locator) {
        Utils.LOG.info("Asserting that element " + locator + " contains text: " + text);
        String textOfPost = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))).getText();
        Assert.assertEquals(text, textOfPost);
    }

    private String getLocatorValueByKey(String locator, Object[] arguments) {
        return String.format(Utils.getUIMappingByKey(locator), arguments);
    }

    public void assertElementNotPresent(String locator) {
        Utils.LOG.info("Asserting that element "+ locator+ " is NOT present on the page");
        Utils.LOG.info("Element "  + locator + " does NOT exist: ");
        Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
    }

}

