package pages.weare;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class WeareHomePage extends BasePage {

    public WeareHomePage(WebDriver driver) {
        super(driver, "weare.homePage");
    }


    public void searchUserByJob() {
        actions.goToUrl("weare.homePage");
        actions.typeValueInField("Chef", "weare.homePage.searchInputJob");
        actions.clickElement("weare.homePage.searchButton");
    }

    public void searchUserByName() {
        actions.goToUrl("weare.homePage");
        actions.typeValueInField("Ivan Manchev", "weare.homePage.searchInputName");
        actions.clickElement("weare.homePage.searchButton");
        actions.waitFor(1000);
    }
    public void searchUserByJobAndName() {
        actions.goToUrl("weare.homePage");
        actions.typeValueInField("Chef", "weare.homePage.searchInputJob");
        actions.typeValueInField("Ivan Manchev", "weare.homePage.searchInputName");
        actions.clickElement("weare.homePage.searchButton");
    }

    public void assertNewPostVisible() {
        actions.waitForElementIsVisible("weare.homePage.newPostButton");

    }

    public void assertLogoutBtnIsNotVisible() {
        actions.assertElementNotPresent("weare.homePage.logoutLink");
    }

    public void checkRegisterBtnOnHeader() {
        actions.clickElement("weare.homePage.registerButton");
    }

    public void checkSignBtnOnHeader() {
        actions.clickElement("weare.homePage.signInButton");
    }

    public void checkAboutUsBtnOnHeader() {
        actions.clickElement("weare.homePage.aboutUsLinkOnHeader");
    }

    public void assertJoinMessageVisible() {
        actions.waitForElementIsVisible("weare.registerPage.joinMessage");
    }

    public void assertUsernameFieldVisible() {
        actions.waitForElementIsVisible("weare.loginPage.usernameField");
    }

    public void assertAboutUsLogoVisible() {
        actions.waitForElementIsVisible("weare.homePage.aboutUsMessage");
    }

    public void assertResultByNameIsVisible() {
        actions.waitForElementIsVisible("weare.sendRequestPage.searchResultByName");
    }

    public void assertResultByJobIsVisible() {
        actions.waitForElementIsVisible("weare.sendRequestPage.searchResultByJob");
    }

    public void assertNoExistingUserMsgVisible() {
        actions.waitForElementIsVisible("weare.homePage.NoExistingUserMessage");
    }


}