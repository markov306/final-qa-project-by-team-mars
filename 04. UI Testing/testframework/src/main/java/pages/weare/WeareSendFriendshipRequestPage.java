package pages.weare;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class WeareSendFriendshipRequestPage extends BasePage {


    public WeareSendFriendshipRequestPage(WebDriver driver) {
        super(driver, "sendRequestPage");
    }

    public void sendFriendshipReq() {
        actions.typeValueInField("Ivan Manchev", "weare.homePage.searchInputName");
        actions.waitFor(500);
        actions.clickElement("weare.homePage.searchButton");
        actions.waitFor(1000);
        actions.clickElement("weare.sendRequestPage.seeUserProfile");
        actions.clickElement("weare.sendRequestPage.clickConnectBtn");
    }

    public void assertConnectionMessageVisible() {
        actions.waitForElementIsVisible("weare.sendRequestPage.connectionMessageVisible");
    }


}
