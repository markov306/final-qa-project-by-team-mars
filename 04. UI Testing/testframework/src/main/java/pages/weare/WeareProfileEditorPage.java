package pages.weare;

import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class WeareProfileEditorPage extends BasePage {

    public WeareProfileEditorPage(WebDriver driver) {
        super(driver, "weare.profileEditorPage");
    }


    public void initialProfileDataUpdate(String firstNameKey, String lastNameKey, String birthDayKey){
        actions.clickElement("weare.homePage.personalProfileLink");
        actions.clickElement("weare.profilePage.editProfileLink");
        actions.typeValueInField(Utils.getConfigPropertyByKey(firstNameKey), "weare.profileEditorPage.userFirstNameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey(lastNameKey), "weare.profileEditorPage.userLastNameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey(birthDayKey), "weare.profileEditorPage.userBirthDayField");
        actions.clickElement("weare.profileEditorPage.updateMyProfileButton");
    }

    public void initialProfileCategoryUpdate(String categoryCodeKey){
        actions.clickElement("weare.homePage.personalProfileLink");
        actions.clickElement("weare.profilePage.editProfileLink");
        actions.selectsDropDownItemByValue(Utils.getConfigPropertyByKey(categoryCodeKey),"weare.profileEditorPage.categoryDropDown");
        actions.clickElement("weare.profileEditorPage.updateCategoryButton");

    }

    public void successfulEditUserDescription() {
        actions.clickElement("weare.homePage.personalProfileLink");
        actions.clickElement("weare.profilePage.editProfileLink");
        actions.typeValueInField(Utils.getConfigPropertyByKey("user.editedDescription"), "weare.profileEditorPage.userDescriptionField");
        actions.clickElement("weare.profileEditorPage.updateMyProfileButton");
    }

    public void rollBackUserDescription() {
        actions.typeValueInField("ring and sword", "weare.profileEditorPage.userDescriptionField");
        actions.clickElement("weare.profileEditorPage.updateMyProfileButton");
    }

    public void successfulEditFirstName() {
        actions.clickElement("weare.homePage.personalProfileLink");
        actions.clickElement("weare.profilePage.editProfileLink");
        actions.typeValueInField(Utils.getConfigPropertyByKey("user.editedFirstName"), "weare.profileEditorPage.userFirstNameField");
        actions.clickElement("weare.profileEditorPage.updateMyProfileButton");
    }

    public void rollBackUserFirstName() {
        actions.typeValueInField("Gandalf", "weare.profileEditorPage.userFirstNameField");
        actions.clickElement("weare.profileEditorPage.updateMyProfileButton");
    }

    public void successfulEditLastName() {
        actions.clickElement("weare.homePage.personalProfileLink");
        actions.clickElement("weare.profilePage.editProfileLink");
        actions.typeValueInField(Utils.getConfigPropertyByKey("user.editedLastName"), "weare.profileEditorPage.userLastNameField");
        actions.clickElement("weare.profileEditorPage.updateMyProfileButton");
    }

    public void rollBackUserLastName() {
        actions.typeValueInField("The Gray", "weare.profileEditorPage.userLastNameField");
        actions.clickElement("weare.profileEditorPage.updateMyProfileButton");
    }

    public void successfulEditCity() {
        actions.clickElement("weare.homePage.personalProfileLink");
        actions.clickElement("weare.profilePage.editProfileLink");
        actions.selectsDropDownItemByValue(Utils.getRandomIntFromRange(1, 1), "weare.profileEditorPage.cityDropDown");
        actions.clickElement("weare.profileEditorPage.updateMyProfileButton");
    }


    public void assertNewUserDescriptionAfterEditProfile() {
        actions.assertTextEquals(Utils.getConfigPropertyByKey("user.editedDescription"), "weare.profileEditorPage.userDescriptionField");
    }

    public void assertNewUserFirstNameAfterEditProfile() {
        actions.waitForElementIsVisible("weare.profileEditorPage.editedUserFirstNameField");
    }

    public void assertNewUserLastNameAfterEditProfile() {
        actions.waitForElementIsVisible("weare.profileEditorPage.editedUserLastNameField");
    }

    public void assertNewCityAfterEditProfile() {
        actions.waitForElementIsVisible("weare.profileEditorPage.editedUserCityField");
    }

}
