package pages.weare;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class WeareExploreAllPostsPage extends BasePage {

    private static final String COMMENT_CONTENT = "COMMENT content info data";

    public WeareExploreAllPostsPage(WebDriver driver) {
        super(driver, "weare.exploreAllPostsPage");
    }


    public void createComment() {
        actions.clickElement("weare.homePage.latestPosts");
        actions.waitFor(500);
        actions.clickElement("weare.exploreAllPostsPage.exploreThisPostsBtn");
        actions.waitFor(500);
        actions.typeValueInField(COMMENT_CONTENT, "weare.exploreAllPostsPage.commentTextField");
        actions.clickElement("weare.exploreAllPostsPage.postCommentBtn");
        actions.waitFor(1000);
        actions.resolveSVG("weare.exploreAllPostsPage.showCommentBtn");
        actions.clickElement("weare.exploreAllPostsPage.showCommentBtn");
    }

    public void assertCommentContentVisible() {
        actions.assertTextEquals(COMMENT_CONTENT, "weare.newPostPage.commentField");
    }

}
