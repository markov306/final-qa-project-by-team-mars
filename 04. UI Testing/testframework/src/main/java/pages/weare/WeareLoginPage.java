package pages.weare;

import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class WeareLoginPage extends BasePage {


    public WeareLoginPage(WebDriver driver) {
        super(driver, "weare.loginPage");
    }


    public void login(String usernameKey, String passwordKey) {
        actions.goToUrl("weare.homePage");
        actions.clickElement("weare.homePage.signInButton");
        actions.typeValueInField(Utils.getConfigPropertyByKey(usernameKey), "weare.loginPage.usernameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey(passwordKey), "weare.loginPage.passwordField");
        actions.clickElement("weare.loginPage.loginButton");
    }

    public void logOut() {
        actions.clickElement("were.homePage.homeLink");
       actions.clickElement("weare.homePage.logoutLink");
    }

    public void assertLogoutMessageVisible() {
        actions.waitForElementIsVisible("weare.loginPage.logoutMessage");

    }
    public void assertWrongPassMsgVisible() {
        actions.waitForElementIsVisible("weare.loginPage.wrongUsrOrPassMsg");
    }

}