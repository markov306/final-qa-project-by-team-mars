package pages.weare;

import com.telerikacademy.testframework.pages.BasePage;
import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class WeareRegisterPage extends BasePage {


    public WeareRegisterPage(WebDriver driver) {
        super(driver, "weare.registerPage");
    }


    public void registrationWithPresetData(String usernameKey, String emailKey, String passwordKey, String confirmPasswordKey) {
        actions.goToUrl("weare.homePage");
        actions.clickElement("weare.homePage.registerButton");
        actions.typeValueInField(Utils.getConfigPropertyByKey(usernameKey), "weare.registerPage.UsernameInput");
        actions.typeValueInField(Utils.getConfigPropertyByKey(emailKey), "weare.registerPage.EmailInput");
        actions.typeValueInField(Utils.getConfigPropertyByKey(passwordKey), "weare.registerPage.PasswordInput");
        actions.typeValueInField(Utils.getConfigPropertyByKey(confirmPasswordKey), "weare.registerPage.PasswordConfirmInput");
        actions.selectsDropDownItemByValue(Utils.getRandomIntFromRange(100, 157), "weare.registerPage.ProfessionsDropDown");
        actions.clickElement("weare.registerPage.RegisterButton");
        actions.clickElement("weare.registerPage.homeBtn");
    }

    public void registration_With_Data_To_be_filled_in(String usernameText, String emailText, String passwordText, String confirmPasswordText) {
        actions.goToUrl("weare.homePage");
        actions.clickElement("weare.homePage.registerButton");
        actions.typeValueInField(usernameText, "weare.registerPage.UsernameInput");
        actions.typeValueInField(emailText, "weare.registerPage.EmailInput");
        actions.typeValueInField(passwordText, "weare.registerPage.PasswordInput");
        actions.typeValueInField(confirmPasswordText, "weare.registerPage.PasswordConfirmInput");
        actions.selectsDropDownItemByValue(Utils.getRandomIntFromRange(100, 157), "weare.registerPage.ProfessionsDropDown");
        actions.clickElement("weare.registerPage.RegisterButton");
    }

    public void successfulRegistration() {
        actions.goToUrl("weare.homePage");
        actions.clickElement("weare.homePage.registerButton");
        actions.typeValueInField(Utils.getRandomString(6), "weare.registerPage.UsernameInput");
        actions.typeValueInField(Utils.getRandomEmail(10), "weare.registerPage.EmailInput");
        actions.typeValueInField(Utils.getConfigPropertyByKey("randomUser.password"), "weare.registerPage.PasswordInput");
        actions.typeValueInField(Utils.getConfigPropertyByKey("randomUser.password"), "weare.registerPage.PasswordConfirmInput");
        actions.selectsDropDownItemByValue(Utils.getRandomIntFromRange(100, 157), "weare.registerPage.ProfessionsDropDown");
        actions.clickElement("weare.registerPage.RegisterButton");
    }

    public void registrationWithEmptyField() {
        actions.goToUrl("weare.homePage");
        actions.clickElement("weare.homePage.registerButton");
        actions.clickElement("weare.registerPage.RegisterButton");
    }


    public void assertWelcomeMessageVisible() {
        actions.waitForElementIsVisible("weare.registerPage.welcomeMessage");
    }

    public void assertJoinMessageVisible() {
        actions.waitForElementIsVisible("weare.registerPage.joinMessage");
    }

    public void assertJoinMessageIsNotVisible() {
        actions.assertElementNotPresent("weare.registerPage.joinMessage");
    }

    public void assertInvalidEmailMessageVisible() {
        actions.waitForElementIsVisible("weare.profilePage.emailErrorMsg");
    }


}