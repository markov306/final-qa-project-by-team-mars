package pages.weare;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;


public class WeareNewPostPage extends BasePage {

    private static final String NEW_POST_CONTENT = "Post Message information";
    private static final String EDIT_POST_CONTENT = "Edit Post content info";


    public WeareNewPostPage(WebDriver driver) {
        super(driver, "weare.newPostPage");
    }


    public void createPost() {
        actions.clickElement("weare.homePage.newPostButton");
        changePostVisibilityToPublic();
        createPostBodyContent();
        actions.clickElement("weare.newPostPage.savePostButton");
    }

    public void likePost() {
        actions.clickElement("weare.homePage.latestPosts");
        actions.clickElement("weare.newPostPage.browsePublicPosts");
        actions.waitFor(1000);
        actions.clickElement("weare.exploreAllPostsPage.likePostBtn");
    }

    public void editPost() {
        createPost();
        actions.clickElement("weare.exploreAllPostsPage.exploreThisPostsBtn");
        actions.clickElement("weare.newPostPage.editUserPostBtn");
        changePostVisibilityToPublic();
        actions.typeValueInField(EDIT_POST_CONTENT, "weare.newPostPage.postMessageField");
        actions.clickElement("weare.newPostPage.savePostButton");
    }

    public void likePostAsGuest() {
        actions.clickElement("weare.homePage.latestPosts");
        actions.clickElement("weare.newPostPage.browsePublicPosts");
    }

    public void viewPublicPost() {
        actions.clickElement("weare.homePage.latestPosts");
        actions.clickElement("weare.newPostPage.browsePublicPosts");
        actions.waitFor(1000);
    }

    public void guestCanViewPublicPost() {
        actions.clickElement("weare.homePage.latestPosts");
    }


    public void createPostBodyContent() {
        actions.waitForElementClickable("weare.newPostPage.postMessageField");
        actions.typeValueInField(NEW_POST_CONTENT, "weare.newPostPage.editUserPostMessField");
    }


    public void changePostVisibilityToPublic() {
        actions.waitForElementClickable("weare.newPostPage.postVisibilityDropMenu");
        actions.clickElement("weare.newPostPage.postVisibilityDropMenu");
        actions.waitForElementClickable("weare.newPostPage.publicPostOption");
        actions.clickElement("weare.newPostPage.publicPostOption");
    }

       public void assertPostContentVisible() {
        actions.assertElementContainsText(NEW_POST_CONTENT, "//p[normalize-space()='Post Message information']");
    }

    public void assertEditPostContentVisible() {
        actions.assertTextEquals(EDIT_POST_CONTENT, "//p[normalize-space()='Edit Post content info']");
    }

    public void assertDislikeBtnVisible() {
        actions.waitForElementIsVisible("weare.exploreAllPostsPage.dislikePostBtn");
        actions.waitFor(1000);
        actions.clickElement("weare.exploreAllPostsPage.dislikePostBtn");
    }

    public void assertLikeBtnIsNotVisible() {
        actions.assertElementNotPresent("weare.homePage.logoutLink");
    }

    public void assertPublicPostIsVisible() {
        actions.waitForElementIsVisible("weare.newPostPage.publicPostIsVisible");
    }

}

