package pages.weare;


import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class WeareAdminPage extends BasePage {

    public WeareAdminPage(WebDriver driver) {
        super(driver, "weare.adminPage");
    }

    private static final String ADMIN_EDIT_USER_FIRSTNAME = "Admin First Name";

    public void adminEditUserFirstName() {
        actions.typeValueInField("Ivan Manchev", "weare.homePage.searchInputName");
        actions.clickElement("weare.homePage.searchButton");
        actions.waitFor(500);
        actions.clickElement("weare.adminPage.seeProfileBtn");
        actions.waitFor(500);
        actions.clickElement("weare.adminPage.editProfileBtn");
        actions.typeValueInField(ADMIN_EDIT_USER_FIRSTNAME, "weare.profileEditorPage.userFirstNameField");
        actions.clickElement("weare.profileEditorPage.updateMyProfileButton");
    }
    public void rollBackAdminEditUserFirstName() {
        actions.typeValueInField("Ivan", "weare.profileEditorPage.userFirstNameField");
        actions.clickElement("weare.profileEditorPage.updateMyProfileButton");
    }

    public void assertNewUserFirstNameAdminEdit() {
        actions.waitForElementIsVisible("weare.adminPage.editedUserFirstNameAdmin");
        actions.typeValueInField("Ivan", "weare.adminPage.editedUserFirstNameAdmin");
        actions.clickElement("weare.profileEditorPage.updateMyProfileButton");
    }

}
