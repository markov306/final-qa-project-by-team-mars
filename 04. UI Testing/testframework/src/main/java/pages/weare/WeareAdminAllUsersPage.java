package pages.weare;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class WeareAdminAllUsersPage extends BasePage {

    private static final String ADMIN_EDIT_POST_CONTENT = "Admin edit Post content info";


    public WeareAdminAllUsersPage(WebDriver driver) {
        super(driver, "weare.adminAllUsersPage");
    }


    public void disableUser() {
        actions.clickElement("weare.homePage.goToAdminZoneLink");
        actions.clickElement("weare.adminPage.viewUsers");
        actions.clickElement("weare.adminAllUsersPage.lastCreatedUserSeeProfile");
        actions.waitFor(500);
        actions.clickElement("weare.profileDisableUserPage.disableButton");
    }

    public void adminEditUserPost() {
        actions.clickElement("weare.homePage.latestPosts");
        actions.clickElement("weare.newPostPage.browsePublicPosts");
        actions.clickElement("weare.exploreAllPostsPage.exploreThisPostsBtn");
        actions.clickElement("weare.newPostPage.editUserPostBtn");
        changePostVisibilityToPublic();
        actions.typeValueInField(ADMIN_EDIT_POST_CONTENT, "weare.newPostPage.postMessageField");
        actions.clickElement("weare.newPostPage.savePostButton");
    }

    public void changePostVisibilityToPublic() {
        actions.waitForElementClickable("weare.newPostPage.postVisibilityDropMenu");
        actions.clickElement("weare.newPostPage.postVisibilityDropMenu");
        actions.waitForElementClickable("weare.newPostPage.publicPostOption");
        actions.clickElement("weare.newPostPage.publicPostOption");
    }

    public void assertAdminEditPostVisible() {
        actions.assertTextEquals(ADMIN_EDIT_POST_CONTENT, "//p[normalize-space()='Admin edit Post content info']");
    }

    public void assertEnableBtnVisible() {
        actions.waitFor(500);
        actions.waitForElementIsVisible("weare.profileDisableUserPage.enableButton");
        actions.clickElement("weare.profileDisableUserPage.enableButton");
    }

}
