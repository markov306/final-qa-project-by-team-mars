# Testing WEare Social Network

## This is Final QA project by Team Mars.

WEare social network is a professional network, where people can exchange experiance and services. WEare all good at what we do. Why not share? Now you can help others with what you are good at and you could use some help from the others. All users and post aim to show to the world, what services are now available and what people are now doing. 

## Resources

1. QA Project Management Board - [Trello link](https://trello.com/b/qRwImtWG/final-project-team-mars)


2. Final Project Repository - [GitLab link](https://gitlab.com/markov306/final-qa-project-by-team-mars)


3. Presentation link : [Canva.com presentation link](https://www.canva.com/design/DAElk0vh9I0/clWV-e9Xkjc7whydrRtvxQ/view?utm_content=DAElk0vh9I0&utm_campaign=designshare&utm_medium=link&utm_source=homepage_design_menu) 


4. Logged issues due testing - [GitLab link](https://gitlab.com/markov306/final-qa-project-by-team-mars/-/issues)


5. Exploratory Testing Report - [OneDrive link](https://telerikacademy-my.sharepoint.com/:x:/r/personal/valentin_markov_a28_learn_telerikacademy_com/_layouts/15/Doc.aspx?sourcedoc=%7BF2BAADAB-1EF2-4E71-805C-D3983FFD1559%7D&file=Exploratory%20Testing%20Summary%20Report.xlsx&action=default&mobileredirect=true)

8. Test case report - [OneDrive link](https://telerikacademy-my.sharepoint.com/:x:/p/valentin_markov_a28_learn/ERN3qxcm1w1KsA2i6ZtWIEMBNcgIPuZbF31od-DSftoMrg?e=sxkiqP)

9. Summary Testing Report - [OneDrive link](https://telerikacademy-my.sharepoint.com/:p:/p/valentin_markov_a28_learn/EeeKFE7kKqlHpHFO8pJjpJ8BkohFZDhgarbE1d-z_0dHVg?e=07PcNH)


10. QA Final Project Requirements - [OneDrive link](https://telerikacademy-my.sharepoint.com/:b:/p/valentin_markov_a28_learn/EdRZa6bly5hIprs9fH2eh3kBWj0aZsRSeqCCOPwbTLkP4g?e=ylWnyI)

11. Developer Project Requirements -  [OneDrive link](https://telerikacademy-my.sharepoint.com/:b:/p/valentin_markov_a28_learn/EQn-vjGqsIhHpFNtv0bw9A0BLZ6uYFFaAt9MVxO9YwtBrg?e=UAZrV0)



***
## Authors
Valentin Markov - [personal repo](https://gitlab.com/markov306/telerik_QA_Valentin_Markov)   
Plamen Dimitrov - [personal repo](https://gitlab.com/dimitrov.plam/alpha-qa-projects) 
